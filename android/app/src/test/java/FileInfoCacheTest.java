package db;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.coolreader.crengine.FileInfo;
import org.coolreader.db.FileInfoCache;


/**
 * @author Akash Varma, SID: 450 077 933
 * @implNote Test cases to test the get(Long id) method of the FileInfoCache class.
 * The tests describe the cyclomatic complexity of 3 which was calculated in the
 * deliverable 08_cyclomatic
 *
 * The tests will validate the following when the get(Long id) method is called:
 *
 * - testCase01: When the passed 'id' parameter is null, the return value
 * 			    should be null.
 *
 * - testCase02: When a file matching the passed 'id' parameter doesn't
 * 			    exist, the return value should be null.
 *
 * - testCase03: If there exists a specific file in the system, whose id
 * 			    matches the passed 'id' parameter, the file should be
 * 			    returned.
 */

public class FileInfoCacheTest {

    private FileInfoCache testFileCache;
    private int maxSize = 10;


    /**
     * @throws java.lang.Exception
     * @implNote creates an instance of the FileInfoCache class and adds FileInfo
     *			objects to an array to allow testing of removing objects from
     *			array
     */
    @Before
    public void setUp() throws Exception {
        testFileCache = new FileInfoCache(maxSize);
        addFileInfoObject();
    }

    /**
     * @implNote helper method that creates instances of FileInfo objects and adds
     * them to the array in the FileInfoCache class.
     */
    public void addFileInfoObject() {
        for (int i = 0; i < maxSize; i++) {
            FileInfo file = new FileInfo();
            file.setId(Long.valueOf(i));
            file.setArcname("");
            file.setPathName("The path for id: " + String.valueOf(i));
            testFileCache.put(file);
        }
    }

    /**
     * @implNote This test ensures that a null id returns a null fileInfo object.
     *  In the cyclomatic complexity graph, this maps to the path
     *  following the nodes: (1, 2, 3, 7)
     */
    @Test
    public void testCase01() {
        Long testId = new Long(100); //creating an id which is non-existent in the filecache
        Assert.assertNull("The returned FileCache should be null", testFileCache.get(testId));
    }

    /**
     * @implNote This test ensures that a non-existent id returns a null fileInfo object.
     *  In the cyclomatic complexity graph, this maps to the path
     *  following the nodes: (1, 2, 4, 5, 3, 7)
     */
    @Test
    public void testCase02() {
        FileInfo file = new FileInfo();
        Long testId = new Long(100);
        file.setId(Long.valueOf(testId)); //setting an id that has not been set to any of the
        //FileInfo objects created in the test class.
        file.setArcname("");
        file.setPathName("This is a path for Id: " + String.valueOf(testId)); //setting the path to a string that has not been set to
        //any of the FileInfo objects created in the test class.

        Assert.assertNull("The returned FileCache should be null", testFileCache.get(testId));

    }

    /**
     * @implNote This test ensures that the correct FileInfo object is retrieved from the FileCache
     *  following the nodes: (1, 2, 4, 5, 6, 7)
     */
    @Test
    public void testCase03() {
        FileInfo file = new FileInfo();
        Long testId = new Long(100);
        file.setId(Long.valueOf(testId)); //setting an id that has not been set to any of the FileInfo objects created in the test class.
        file.setArcname("");
        file.setPathName("This is a path for Id: " + String.valueOf(testId)); //setting the path to a string that has not been set to any of the FileInfo objects created in the test class.

        testFileCache.put(file);
        Assert.assertNotNull("The returned FileCache should not be null", testFileCache.get(testId));
        Assert.assertSame(file, testFileCache.get(testId)); //The returned object should be the same as 'file'

    }


}
