
import android.graphics.Bitmap;
import android.widget.ImageView;

import org.coolreader.crengine.CoverpageManager;
import org.coolreader.crengine.CoverpageManager.*;
import org.coolreader.crengine.FileInfo;

import org.coolreader.db.CRDBService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;


import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Random;
import java.lang.reflect.Method;


/**
 * @author Akash Varma, SID: 450 077 933
 * @implNote Test cases 01-08 test the BitmapCache class.
 *           Test cases 09-   test the BitmapCache and CoverpageManager classes combined
 *
 * The tests will validate the following when the get(Long id) method is called:
 *
 * - testCase01: testing the void moveOnTop(int index) method
 *
 * - testCase02: testing the int find(ImageItem file) method
 *
 * - testCase03: testing the void checkMaxSize() method
 *
 * - testCase04: testing the void clear() method
 *
 * - testCase05: testing the BitmapCacheItem getItem(ImageItem file) method
 *
 * - testCase06: testing the BitmapCacheItem addItem(ImageItem file) method
 *
 * - testCase07: testing the void unqueue(ImageItem) and void remove(ImageItem) methods
 *
 * - testCase08: testing the Bitmap getBitmap(ImageItem file) method
 *
 * - testCase09: testing the queueForDrawing() method of CoverpageManager class
 *
 * - testCase10: testing the setItemState() method for CoverpageManager class
 */

public class BitmapCacheTest {

    private CoverpageManager testCoverpageManager;
    private BitmapCache testBitmapCache;
    int testMaxSize = 64;
    ImageItem[] arrayImageItem = new ImageItem[testMaxSize];
    Random rand = new Random();

    /**
     * @implNote creates an instance of CoverpageManager object and its BitmapCache attribute
     */
    @Before
    public void setUp() {
        //testCoverpageManager = mock(CoverpageManager.class);
        testCoverpageManager = new CoverpageManager();
        testBitmapCache = testCoverpageManager.new BitmapCache(testMaxSize);
        this.populateBitmapCache(testMaxSize);
    }

    /**
     * @implNote helper method that creates ImageItem objects then adds them to the BitMapCache object.
     */
    private void populateBitmapCache(int maxSize) {

        for (int i = 0; i < maxSize; i++) {

            int maxHeight = rand.nextInt(1000) + 1; //random height between 1 and 1000
            int maxWidth = rand.nextInt(1000) + 1; //random width between 1 and 1000

            ImageItem item = new ImageItem(new FileInfo(), maxHeight, maxWidth);
            arrayImageItem[i] = item;
            //BitmapCacheItem newItem = testCoverpageManager.new BitmapCacheItem(item);
            testBitmapCache.addItem(item);
        }
    }

    /**
     * @implNote Tests the moveOnTop(int index) method for:
     * 			~ invalid negative index supplied as parameter
     * 			~ index greater than the size of the BitmapCache object
     * 			~ index within the size of the BitmapCache object
     */
     @Test (expected = IndexOutOfBoundsException.class)
     public void testCase01() {

         /**
          * Test for index greater than the maximum allowed size
          */
         {
             ImageItem itemOne = arrayImageItem[0]; //the ImageItem object currently at the top

             int beforeIndex = testBitmapCache.find(itemOne);

             testBitmapCache.moveOnTop(testMaxSize + 1); //index out of bounds, so should have no effect

             int afterIndex = testBitmapCache.find(itemOne);

             Assert.assertEquals(beforeIndex, afterIndex);
         }

         /**
          * Test for invalid (negative) index
          */
         {
             ImageItem itemOne = arrayImageItem[0]; //the ImageItem object currently at the top

             int beforeIndex = testBitmapCache.find(itemOne);

             testBitmapCache.moveOnTop(-1); //negative index throws exception, no effect on BitmapCache object

             int afterIndex = testBitmapCache.find(itemOne);

             Assert.assertEquals(beforeIndex, afterIndex);
         }

         /**
          * Test valid index
          */
         {
             int moveIndex = rand.nextInt(testMaxSize - 1); //index to be moved to top

             ImageItem oldTop = arrayImageItem[0]; //the ImageItem object currently at top

             ImageItem newTop = arrayImageItem[moveIndex]; //ImageItem object to be moved to top

             testBitmapCache.moveOnTop(moveIndex);

             Assert.assertEquals(testMaxSize - 1, testBitmapCache.find(newTop));
             Assert.assertNotEquals(testMaxSize - 1, testBitmapCache.find(oldTop));

             Assert.assertEquals(1, testBitmapCache.find(oldTop));
             Assert.assertNotEquals(moveIndex, testBitmapCache.find(newTop));

         }
     }

    /**
     * @implNote Tests the int find(ImageItem file) method for:
     * 			~ null ImageItem object supplied as a parameter
     * 			~ valid ImageItem object that does not exist in the BitmapCache object
     * 			~ valid ImageItem object that exists in the BitmapCache object
     */
    @Test
    public void testCase02() {

        ImageItem itemOne = null; //null ImageItem object to test null parameter
        try {
            Assert.assertNull(testBitmapCache.find(itemOne));
        } catch (NullPointerException e) {
            System.out.printf("%s", "null pointer returned");
        }

        ImageItem itemTwo = new ImageItem(new FileInfo(), 110, 110); //object not present in BitmapCache object
        Assert.assertEquals(-1, testBitmapCache.find(itemTwo));

        //looping through the present ImageItem objects to test their presence in the BitmapCache
        for (int i = 0; i < testMaxSize; i++) {

            Assert.assertEquals(i, testBitmapCache.find(arrayImageItem[i]));
        }
    }

    /**
     * @implNote helper method to add additional elements to the BitmapCache object
     * @param target the BitmapCache object to which more elements are added
     * @param numAdded the number of new elements to add
     */
    private void addMoreElements(BitmapCache target, int numAdded) {

        for(int i = 0; i < numAdded; i++) {
            int maxHeight = rand.nextInt(1000) + 1; //random height between 1 and 1000
            int maxWidth = rand.nextInt(1000) + 1; //random width between 1 and 1000

            ImageItem item = new ImageItem(new FileInfo(), maxHeight, maxWidth);
            testBitmapCache.addItem(item);
        }

    }

    /**
     * @implNote helper method to add additional elements to the BitmapCache object
     * @param target the BitmapCache object from which elements are removed
     */
    private void removeSomeElements(BitmapCache target) {

        int numToRemove = target.size() / 2;

        for(int i = 0; i < numToRemove; i++) {

            target.remove(arrayImageItem[i]);
        }

    }

    /**
     * @implNote Tests the void checkMaxSize() method for:
     * 			~ full BitmapCache object
     * 		    ~ empty BitmapCache object
     * 		    ~ semi full BitmapCache object
     */
    @Test
    public void testCase03() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        Class cls = testBitmapCache.getClass();

        Method checkMaxSize = cls.getDeclaredMethod("checkMaxSize");

        checkMaxSize.setAccessible(true);

         //full BitmapCache object
        {
            int oldSize = testBitmapCache.size();

            checkMaxSize.invoke(testBitmapCache); //when size == maxSize

            int newSize = testBitmapCache.size();

            Assert.assertEquals(oldSize, newSize);
        }


         //empty BitmapCache object
        {
            BitmapCache newCache = new CoverpageManager().new BitmapCache(rand.nextInt(10) + 1);

            int oldSize = newCache.size();

            checkMaxSize.invoke(newCache); // empty BitmapCache object

            int newSize = newCache.size();

            Assert.assertEquals(oldSize, newSize);
        }


         //semi full BitmapCache object
        {
            this.removeSomeElements(testBitmapCache);

            int oldSize = testBitmapCache.size();

            checkMaxSize.invoke(testBitmapCache);

            int newSize = testBitmapCache.size();

            Assert.assertEquals(oldSize, newSize);

        }

    }

    /**
     * @implNote Tests the void clear() method for:
     * 			~ full BitmapCache object
     * 		    ~ semi full BitmapCache object
     * 		    ~ empty BitmapCache object
     */
    @Test
    public void testCase04() {

        //full BitmapCache object
        {
            int oldSize = testBitmapCache.size();

            testBitmapCache.clear();

            int newSize = testBitmapCache.size();

            Assert.assertNotEquals(oldSize, newSize);

            Assert.assertEquals(0, newSize);
        }

        //semi full BitmapCache object
        {
            this.setUp();

            this.removeSomeElements(testBitmapCache);

            int oldSize = testBitmapCache.size();

            testBitmapCache.clear();

            int newSize = testBitmapCache.size();

            Assert.assertNotEquals(oldSize, newSize);

            Assert.assertEquals(0, newSize);
        }

        //Empty BitmapCache object
        {
            BitmapCache newCache = new CoverpageManager().new BitmapCache(rand.nextInt(10) + 1);

            int oldSize = newCache.size();

            newCache.clear();

            int newSize = newCache.size();

            Assert.assertEquals(oldSize, newSize);

            Assert.assertEquals(0, newSize);
        }
    }

    /**
     * @implNote Tests the BitmapCacheItem getItem(ImageItem file) method by checking change in index
     *
     */
    @Test
    public void testCase05() {


        int oldIndex = rand.nextInt(testMaxSize - 1) + 1; //random index between 1 and maxSize

        ImageItem item = arrayImageItem[oldIndex]; //retrieving ImageItem object at oldIndex

        testBitmapCache.getItem(item);

        int newIndex = testBitmapCache.find(item);

        //element moved to top of BitmapCache
        Assert.assertNotEquals(oldIndex, newIndex);

        Assert.assertEquals(testMaxSize - 1, newIndex);

    }

    /**
     * @implNote Tests the BitmapCacheItem addItem(ImageItem file) method by checking change in index
     */
    @Test
    public void testCase06() {

        ImageItem item = new ImageItem(new FileInfo(), rand.nextInt(testMaxSize - 1) + 1, rand.nextInt(testMaxSize - 1) + 1);

        ImageItem itemTwo = arrayImageItem[0];

        testBitmapCache.addItem(item);

        Assert.assertEquals(-1, testBitmapCache.find(itemTwo)); //element at index 0 lost

        Assert.assertEquals(testMaxSize - 1, testBitmapCache.find(item)); //new element added to top

    }

    /**
     * @implNote Tests the void unqueue(ImageItem file) and void remove(ImageItem) methods by checking change in index
     *
     */
    @Test
    public void testCase07() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        ImageItem image = new ImageItem(new FileInfo(), rand.nextInt(100) + 1, rand.nextInt(100) + 1);

        BitmapCacheItem itemOne = testBitmapCache.addItem(image);

        //Reflecting private method for testing
        Class cls = itemOne.getClass();

        Method setState = cls.getDeclaredMethod("setState", String.class);

        setState.setAccessible(true);

        //test allowed dequeuing of "LOAD_SCHEDULED" states
        {
            testBitmapCache.remove(arrayImageItem[0]);

            setState.invoke(itemOne, "l");

            testBitmapCache.unqueue(image);

            int index = testBitmapCache.find(image);

            Assert.assertEquals(-1, index);
        }

        //test not-allowed dequeuing of "DRAWING" states
        {
            BitmapCacheItem itemTwo = testBitmapCache.addItem(image);

            setState.invoke(itemTwo, "d");

            testBitmapCache.unqueue(image);

            int indexTwo = testBitmapCache.find(image);

            Assert.assertNotEquals(-1, indexTwo); //still present in encompassing BitmapCache object
        }
    }

    /**
     * @implNote Tests the Bitmap getBitmap(ImageItem file) method by checking change in index
     */
    @Test
    public void testCase08() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        ImageItem image = new ImageItem(new FileInfo(), rand.nextInt(100) + 1, rand.nextInt(100) + 1);

        BitmapCacheItem itemOne = testBitmapCache.addItem(image);

        //Reflecting private method for testing
        Class cls = itemOne.getClass();
        Method setBitmap = cls.getDeclaredMethod("setBitmap", Bitmap.class);
        setBitmap.setAccessible(true);

        Bitmap newMap = mock(Bitmap.class);

        //invoking reflected void setBitmap(Bitmap) method
        setBitmap.invoke(itemOne, newMap);


        Assert.assertEquals(newMap, testBitmapCache.getBitmap(image));

    }

    //Mock a CoverpageManager object
    @Mock
    private final CoverpageManager mockCoverpageManager = Mockito.mock(CoverpageManager.class);



    /**
     * @implNote Tests the boolean draw(ImageItem, byte[]) method in CoverpageManager
     */
    @Test
    public void testCase09() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        ImageItem image = null;
        byte[] b = new byte[10];

        Class cls1 = testCoverpageManager.getClass();
        Method draw = cls1.getDeclaredMethod("draw", ImageItem.class, byte[].class);
        draw.setAccessible(true);

        //Calling the draw method with a null ImageItem argument returns a false
        boolean bool = ((Boolean) draw.invoke(testCoverpageManager, image, b)).booleanValue();
        Assert.assertFalse(bool);

        //Initialise dummy objects
        image = new ImageItem(new FileInfo(), -1, -1);
        BitmapCacheItem newItem = testCoverpageManager.new BitmapCacheItem(image);

        //Reflecting the private void setState(String) method of BitmapCacheItem
        Class cls2 = newItem.getClass();
        Method setState = cls2.getDeclaredMethod("setState", String.class);
        setState.setAccessible(true);
        setState.invoke(newItem, "d");

        //Calling draw with ImageItem object in 'Drawing' or 'Ready' state should return false
        bool = ((Boolean) draw.invoke(testCoverpageManager, image, b)).booleanValue();
        Assert.assertFalse(bool);

        setState.invoke(newItem, "r");
        bool = ((Boolean) draw.invoke(testCoverpageManager, image, b)).booleanValue();
        Assert.assertFalse(bool);

        //Calling the method with zero initialised byte array should return false
        setState.invoke(newItem, "f");
        bool = ((Boolean) draw.invoke(testCoverpageManager, image, b)).booleanValue();
        Assert.assertFalse(bool);

    }

    /**
     * @implNote Tests the BitmapCacheItem setItemState(ImageItem file, State state) method
     */
    @Test
    public void testCase10() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, AssertionError {

        //reflection to access the private method
        Class cls1 = testCoverpageManager.getClass();
        Method setItemState = cls1.getDeclaredMethod("setItemState", ImageItem.class, State.class);
        setItemState.setAccessible(true);

        /**
         * setItemState called with an ImageItem object in the BitmapCache object
         */
        {

            BitmapCacheItem item = (BitmapCacheItem) setItemState.invoke(testCoverpageManager, this.arrayImageItem[0], State.FILE_CACHE_LOOKUP);

            //calling setItemState with an existing ImageItem object should set the state of the object as the passed state
            Assert.assertEquals(State.FILE_CACHE_LOOKUP, item.getState());

            //the BitmapCacheItem object corresponding to the ImageItem object should be at the top of the BitmapCache object
            Assert.assertEquals(0, testBitmapCache.find(this.arrayImageItem[0]));

        }



        /**
         * setItemState called with an ImageItem object NOT in the BitmapCache object
         */
        {

            ImageItem image = new ImageItem(new FileInfo(), 20, 13);

            Assert.assertEquals(-1, testBitmapCache.find(image));

            this.removeSomeElements(testBitmapCache);

            BitmapCacheItem item = (BitmapCacheItem) setItemState.invoke(testCoverpageManager, image, State.DRAWING);

            //state attribute of the returned BitmapCache object should equal the passed State enum type
            Assert.assertEquals(item.getState(), State.DRAWING);

            //the ImageItem object should now exist in the BitmapCache object
            //bug in the code for CoolReader, the test fails
            Assert.assertNotEquals(-1, testBitmapCache.find(image));
        }

    }


}

